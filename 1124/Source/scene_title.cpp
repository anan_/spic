
#include "scene_title.h"
#include "scene_manager.h"
#include "create_DX11.h"
using namespace GetCoreSystem;
#include "misc.h"
#include <string>
#include <sstream>
#include <iostream>
#include "input.h"
#include "load_model.h"
#include "sound_data.h"
#include "font.h"
#include "character_manager.h"
#include "enemy_manager.h"
#include "shot_manager.h"
#include "judge.h"
#include "collision.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

void SceneTitle::CreateLight(ID3D11Device * device)
{
	Light::Init(device);
	lightBuffer = std::make_unique<ConstantBuffer<CbLight>>(device);
}

void SceneTitle::Init(ID3D11Device* device)
{
	camera = std::make_shared<Camera>();
	CreateLight(device);
	renderer = std::make_unique<ModelRenderer>(device);

	title_img = std::make_unique<Sprite>(device, L"Data/fbx/sonota/title.png");

	//Font::Create();
	//Font::GetInstance().Load(device);

	//SoundData::Create();

	//   "Data/fbx/danbo_fbx/danbo_atk.fbx"
	//   "Data/fbx/cubes/001_cube.fbx"
	//   "Data/fbx/zubat/source/zubat.fbx"
	//    "Data/fbx/sonota/chara2.fbx"

	/************************/
	//ボディモデルロード
	playerBodyMove = std::make_shared<PlayerBody>();
	playerBodyMove->SetCamera(camera);
	pCharacterManager.Create(device);
	pCharacterManager.Add(device, pLoadModel.GetModelResource("Player_body"),
		playerBodyMove.get(),
		VECTOR3(0.5f, 0.5f, 0.5f), VECTOR3(0, 0, 0), VECTOR3(0, 0, 0));
	///************************/
	////ヘッドモデルロード

	playerGunMove = std::make_shared<PlayerGun>();
	playerGunMove->SetCamera(camera);
	pCharacterManager.Add(device, pLoadModel.GetModelResource("Player_head"),
		playerGunMove.get(),
		VECTOR3(0.5f, 0.5f, 0.5f), VECTOR3(0, 0, 0), VECTOR3(0, 0, 0));

	pShotManager.Create(device);
	field = std::make_unique<Obj3D>();
	field->SetModelResource(pLoadModel.GetModelResource("Field"));
	field->SetMoveAlg(stageMove.get());
	field->SetScale(VECTOR3(50.f, 50.f, 50.f));
	field->SetRotation(VECTOR3(0, 0, 0));
	field->SetPosition(VECTOR3(0, -50.f, 0.0f));
	Collision::RegisterTerrain(field.get());

	//SoundData::GetInstance().Load();

	//lightBuffer = std::make_unique<ConstantBuffer<CbLight>>(device);
}

void SceneTitle::Release()
{
	//pCharacterManager.Destory();
	pShotManager.Destory();
	//SoundData::Destory();
	//Font::Destory();
}

void SceneTitle::Update(ID3D11Device* device, float elapsedTime)
{
	camera->Update();
	field->Update(elapsedTime);
	pCharacterManager.Update(elapsedTime);
	pShotManager.Update(elapsedTime);
	float XM_PI = 3.141592654f;
	static float lightAngle = XM_PI;
	Light::SetAmbient(VECTOR3(0.5f, 0.5f, 0.5f));
	//ライト方向
	LightDir.x = sinf(lightAngle);
	//でっけえライト
	LightDir.y = 0.0f;
	LightDir.z = cosf(lightAngle);
	static float angle = XM_PI / 4;
	angle += elapsedTime;
	Light::SetDirLight(LightDir, VECTOR3(1.0f, 1.0f, 1.0f));

	float s = sinf(angle)*1.0f;
	float c = cosf(angle)*1.0f;

	{
		auto pl = pCharacterManager.GetList()->begin();
		{
			VECTOR3 start(pl->GetPosition().x, pl->GetPosition().y + 200.f, pl->GetPosition().z);
			VECTOR3 end(pl->GetPosition().x, pl->GetPosition().y - 200.f, pl->GetPosition().z);
			VECTOR3 outPos, outNormal;

			if (-1 != Collision::RayPick(start, end, &outPos, &outNormal))
			{
				pl->SetPosition(outPos);
			}
		}

		{
			//プレイヤーヘッド
			auto plhead = &(*std::next(pCharacterManager.GetList()->begin(), 1));
			//plhead++;
			plhead->SetPosition(VECTOR3(pl->GetPosition().x, pl->GetPosition().y - 0.5f, pl->GetPosition().z));
		}
		for (auto& shot : *pShotManager.GetList())
		{
			erasePlayerShot = std::make_unique<ErasePlayerShot>();

			shot.SetEraseAlg(erasePlayerShot.get());
		}
		/*if (-1 != Collision::RayPick(start, end, &outPos, &outNormal))
		{君は後で
		pl->SetPosition(outPos);
		}*/
		static Key SPACE(VK_RETURN);
		if (SPACE.state(TRIGGER_MODE::RISINING_EDGE))
		{
			SceneManager::GetInstance().ChangeScene(device, ToSTRING(GAME));
		}
	}
}

void SceneTitle::Render(ID3D11DeviceContext* context, float elapsedTime)
{

	SetRender::SetBlender(BS_ALPHA);
	SetRender::SetDepthStencilState(DS_TRUE);
	SetRender::SetRasterizerState(RS_CULL_BACK);

	lightBuffer->data.ambientColor = Light::Ambient;
	lightBuffer->data.lightDir = Light::LightDir;
	lightBuffer->data.lightColor = Light::DirLightColor;
	lightBuffer->data.eyePos.x = camera->GetEye().x;
	lightBuffer->data.eyePos.y = camera->GetEye().y;
	lightBuffer->data.eyePos.z = camera->GetEye().z;
	lightBuffer->data.eyePos.w = 1.0f;

	memcpy(lightBuffer->data.PointLight, Light::PointLight, sizeof(POINTLIGHT)*Light::POINTMAX);
	memcpy(lightBuffer->data.SpotLight, Light::SpotLight, sizeof(SPOTLIGHT)*Light::SPOTMAX);


	// モデルの描画
	{
		lightBuffer->Activate(context, 3);
		renderer->Begin(context, camera->GetViewProection());

		for (auto &it : *pCharacterManager.GetList())
		{
			renderer->Draw(context, it.GetModel());
		}
		renderer->Draw(context, field->GetModel());
		//renderer->Draw(context, sky->GetModel());
		float dot = 0;
		float acs = 0;


		renderer->Draw(context, playerBodyMove->GetCubeModel(), VECTOR4(1, 1, 1, 0.5f));

		renderer->End(context);

		pShotManager.Render(context, camera->GetViewProection());

		lightBuffer->DeActivate(context);
	}
	title_img->Render(context,
		VECTOR2(0, 0), VECTOR2(1920, 1080),
		VECTOR2(0, 0), VECTOR2(1920, 1080),
		1.0f);
}