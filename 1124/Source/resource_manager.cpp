
#include "resource_manager.h"
#include <fstream>
#include "WICTextureLoader.h"
#include <assert.h>
#include "fbx_loader.h"

std::map<std::wstring, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> ResourceManager::textures;
std::map<std::string, ResourceManager::VertexShader_and_InputLayout>     ResourceManager::vertex_shader;
std::map<std::string, Microsoft::WRL::ComPtr<ID3D11PixelShader>>         ResourceManager::pixel_shader;

bool LoadFile(const char* filename, char** blob, unsigned int& size)
{
    FILE* fp = nullptr;
    fopen_s(&fp, filename, "rb");

	if (fp == nullptr)
	{
		assert(!"NotFound CSO File");
        return false;
	}


    fseek(fp, 0, SEEK_END);
    size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    *blob = new char[size];
    fread(*blob, size, 1, fp);
    fclose(fp);

    return true;
}

//シェーダー読み込み
bool ResourceManager::LoadShaderResourceView(ID3D11Device* device, const wchar_t* filename,
    ID3D11ShaderResourceView** SRView, D3D11_TEXTURE2D_DESC* texDecs)
{
    ID3D11Resource* resource = nullptr;
    std::wstring wstr(filename);

    //データ探索
    std::map <std::wstring, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>::iterator it = textures.find(wstr);
    if (it != textures.end())
    {
        //見つけたら
        *SRView = it->second.Get();
        it->second.Get()->GetResource(&resource);
        (*SRView)->AddRef();
    }
    else
    {
        HRESULT hr = S_OK;
        hr = DirectX::CreateWICTextureFromFile(device, filename, &resource, SRView);
        if (FAILED(hr))
        {
            assert(!"CreateWICTextureFromFile error");
            return false;
        }
        //mapに入れるやつ と　作るやつ
        textures.insert(std::make_pair(wstr, *SRView));
    }

    //texture2Dの取得 検索
    ID3D11Texture2D* tex2d;
    if (FAILED(resource->QueryInterface(&tex2d)))
    {
        assert(!"QueryInterface error");
        resource->Release();
        return false;
    }
    tex2d->GetDesc(texDecs);

    tex2d->Release();
    resource->Release();
    return true;
}

void ResourceManager::ReleaseShaderResourceView(std::wstring wstr)
{
    //auto& it = textures.find(wstr);
    //if (it != textures.end())
    //{
    //    textures.erase(it);
    //}
    //else
    //{
    //    assert(!"ShaderResourceView :No File");
    //}
	textures.clear();
}

//頂点シェーダー読み込み
bool ResourceManager::LoadVertexShaders(ID3D11Device* device, const char* filename,
    D3D11_INPUT_ELEMENT_DESC * elementDescs, int numElement,
    ID3D11VertexShader** vs, ID3D11InputLayout** il)
{
    *vs = nullptr;
    *il = nullptr;

    //csoファイルのロード
    std::map < std::string, ResourceManager::VertexShader_and_InputLayout>::iterator it = vertex_shader.find(filename);
    //データ探索
    if (it != vertex_shader.end())
    {
        *vs = it->second.vertexShader.Get();
        (*vs)->AddRef();

        *il = it->second.inputLayout.Get();
        (*il)->AddRef();

        return true;
    }

    char* blob;
    unsigned int size = 0;

    //コンパイル済みピクセルシェーダーオブジェクトの読み込み
    if (!LoadFile(filename, &blob, size))
    {
        assert(!"LoadFile");
        return false;
    }

    HRESULT hr = S_OK;
    //頂点シェーダーオブジェクトのせいせい
    hr = device->CreateVertexShader(blob, size, nullptr, vs);
    if (FAILED(hr))
    {
        OutputDebugStringA("***********************");
        OutputDebugStringA("CreateVertexShader でエラー");
        OutputDebugStringA("***********************");
        assert(!"CreateVertexShader error");
        return false;
    }

    // 入力レイアウトの作成
    hr = device->CreateInputLayout(elementDescs, numElement,
        blob, size, il);
    if (FAILED(hr))
    {
        OutputDebugStringA("***********************");
        OutputDebugStringA("CreateInputLayout でエラー");
        OutputDebugStringA("***********************");
        assert(!"CreateInputLayout error");
        return false;
    }
    delete[] blob;

    vertex_shader.insert(std::make_pair(filename, VertexShader_and_InputLayout(*vs, *il)));
    return true;
}

void ResourceManager::ReleaseVertexShaders(std::string str)
{
    //std::map < std::string, ResourceManager::VertexShader_and_InputLayout>::iterator it = vertex_shader.find(str);
    //if (it != vertex_shader.end())
    //{
    //    vertex_shader.erase(it);
    //}
    //else
    //{
    //    assert("VertexShader :No File");
    //}
	vertex_shader.clear();
}

bool ResourceManager::LoadPixelShaders(ID3D11Device* device, const char* filename,
    ID3D11PixelShader** ps)
{
    *ps = nullptr;

    //csoファイルのロード
    std::map<std::string, Microsoft::WRL::ComPtr<ID3D11PixelShader>>::iterator it = pixel_shader.find(filename);
    if (it != pixel_shader.end())
    {
        *ps = it->second.Get();
        (*ps)->AddRef();
        return true;
    }

    char* blob;
    unsigned int size = 0;

    //コンパイル済みピクセルシェーダーオブジェクトの読み込み
    if (!LoadFile(filename, &blob, size))
    {
        assert(!"LoadFile");
        return false;
    }

    HRESULT hr = device->CreatePixelShader(blob, size, nullptr, ps);
    if (FAILED(hr))
    {
        OutputDebugStringA("***********************");
        OutputDebugStringA("CreatePixelShader でエラー");
        OutputDebugStringA("***********************");
        assert("CreatePixelShader error");
        return false;
    }
    delete[] blob;

    pixel_shader.insert(std::make_pair(filename, *ps));
    return true;
}

void ResourceManager::ReleasePixelShaders(std::string str)
{
    //auto& it = pixel_shader.find(str);
    //if (it != pixel_shader.end())
    //{
    //    pixel_shader.erase(it);
    //}
    //else
    //{
    //    assert("PixelShader :No File");
    //}
	pixel_shader.clear();
}


