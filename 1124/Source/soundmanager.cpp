#include"soundmanager.h"

SoundManager::SoundManager()
{
	SoundData sounddata[]=
	{
		{ SOUND::decision,"Data/sound/decision23 (online-audio-converter.com).wav" },
		{ SOUND::BGM_Title,"Data/sound/title.wav" },
		{ SOUND::BGM_Select,"Data/sound/select.wav" },
		{ SOUND::BGM_Game,"Data/sound/game.wav" },
		{ SOUND::BGM_Result,"Data/sound/result.wav"},
		{ SOUND::Count,"Data/sound/count.wav" },
	    { SOUND::Des,"Data/sound/magic-chant2.wav"}
	};
	int size = sizeof(sounddata) / sizeof(SoundData);
	for (int i = 0; i < size; i++)
	{
		mSound.emplace_back(std::make_unique<Sound>(sounddata[i].soundfile));
	}
}