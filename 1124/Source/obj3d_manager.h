#pragma once

#include "obj3d.h"
#include "model_renderer.h"
#include <list>

class Obj3DManager
{
protected:
    std::list<Obj3D> objList;
    std::unique_ptr<ModelRenderer> renderer;
public:
    Obj3DManager(ID3D11Device* device);
    ~Obj3DManager() {}

    void Update(float elapsedTime);
    void Render(ID3D11DeviceContext* context, const FLOAT4X4& viewProjection);

    Obj3D* Add(ID3D11Device* device, std::shared_ptr<ModelResource>& modelResource, MoveAlg* mvAlg,
        const VECTOR3& scale, const VECTOR3& rotation, const VECTOR3& position, bool exist = true);

    std::list<Obj3D>* GetList()
    {
        //if (objList.size() > 0)
        //{
            return &objList;
        //}
        //else
        //{
        //    assert(!"No List");
        //    return nullptr;
        //}
    }
};

//#define pObj3DManager 
