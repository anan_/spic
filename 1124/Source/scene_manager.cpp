
#include "scene_manager.h"

#include "scene_title.h"
#include "scene_game.h"
#include "scene_clear.h"
#include "scene_over.h"

#include "load_model.h"
#include "sound_data.h"

SceneManager* SceneManager::instance = nullptr;
std::map<std::string, std::unique_ptr<Scene>> SceneManager::scenes;

SceneManager::SceneManager(HINSTANCE hInstance, Create_DX11* _create_dx11, ID3D11Device* device)  : create_dx11(_create_dx11)
{
    scenes[ToSTRING(TITLE)] = std::make_unique<SceneTitle>();
    scenes[ToSTRING(GAME)] = std::make_unique<SceneGame>();
    scenes[ToSTRING(CLEAR)] = std::make_unique<SceneClear>();
    scenes[ToSTRING(OVER)] = std::make_unique<SceneOver>();


    //input::GamepadInitialize(0, false, hInstance);
	//pLoadModel.Load(device, "Data/fbx/cubes/001_cube.fbx", "Cube");
	//pLoadModel.Load(device, "Data/fbx/sonota/tank_body.fbx", "Player_body");
	//pLoadModel.Load(device, "Data/fbx/sonota/tank_head.fbx", "Player_head");
	//pLoadModel.Load(device, "Data/fbx/enemy/star.fbx", "Slime");
	//pLoadModel.Load(device, "Data/fbx/sphere/sphere.fbx", "Shot");
	pLoadModel.Load(device, "Data/fbx/sonota/source/Map/A_city.fbx", "Field");
    pLoadModel.Load(device, "Data/fbx/kitchen/kitchen.fbx", "Kitchen");
	//pLoadModel.Load(device, "Data/fbx/sonota/cube.fbx", "Sky");
    pLoadModel.Load(device, "Data/fbx/hand.fbx", "Hand");


    current_scene = ToSTRING(GAME);
    scenes[current_scene]->Init(device);
}

void SceneManager::Execute(HINSTANCE hInstance, ID3D11DeviceContext* context, ID3D11Device* device)
{

    while (create_dx11->GameLoop())
    {
        //input::GamepadUpdate();
        scenes[current_scene]->Update(device,create_dx11->GetElapsedTime());

        create_dx11->RenderingBegin();
        scenes[current_scene]->Render(context, create_dx11->GetElapsedTime());
        create_dx11->RenderingEnd();

        const float average_rendering_time = create_dx11->GetAverageRenderingTime();
    }
}

void SceneManager::ChangeScene(ID3D11Device* device, const char* next_scene)
{
    scenes[current_scene]->Release();

    current_scene = next_scene;
    scenes[current_scene]->Init(device);

    create_dx11->ResetHighResolutionTimer();
}

void SceneManager::Release()
{
	scenes[ToSTRING(TITLE)]->Release();
	scenes[ToSTRING(GAME)]->Release();
	scenes[ToSTRING(CLEAR)]->Release();
	scenes[ToSTRING(OVER)]->Release();
}
