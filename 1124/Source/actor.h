#pragma once

#include "obj3d.h"
#include "camera.h"
//#include "cube.h"

struct ActorParam
{
	VECTOR3 velocity;
	float maxSpeed;
	float acceleration;
	float deceleration;
	float turnSpeed;
};

//struct CharaParam
//{
//	int hp;
//};

class Actor : public MoveAlg
{
public:
	Actor();
	void AddForce(const VECTOR3& force) { pram.velocity += force * pram.acceleration; }
	void SetCamera(std::shared_ptr<Camera>& camera) { this->camera = camera; };

	const Obj3D* GetCube() const { return cube.get(); }
	Model* GetCubeModel() { return cube->GetModel(); }
	//bodyかheadか識別用　
	int parts;
	int Setparts(int parts) { return this->parts = parts; };

protected:
	ActorParam pram;
	std::shared_ptr<Camera> camera;
	std::unique_ptr<Obj3D> cube;

	VECTOR2 oldCursor, newCursor;

	void SetAcceleration(float acceleration) { pram.acceleration = acceleration; }
	void SetDeceleration(float deceleration) { pram.deceleration = deceleration; }
	void SetMaxSpeed(float maxSpeed) { pram.maxSpeed = maxSpeed; }

	void SetCube(const VECTOR3& scale, const VECTOR3& rotation, const VECTOR3& position);

	void MainMove(Obj3D* obj, float elapsedTime);

	// これを呼ぶ
	void PlayerBodyMove(Obj3D* obj, float elapsedTime);
	void PlayerGunMove(Obj3D* obj, float elapsedTime);
	void PlayerShotMove(Obj3D* obj, float elapsedTime);
	void ShotMove(Obj3D* obj, float elapsedTime);
	//void SkyMove(Obj3D* obj, float elapsedTime);
	void EnemyMove(Obj3D* obj, float elapsedTime);
};


