#pragma once

#include <d3d11.h>
#include <wrl.h>
#include <memory>
#include "vector.h"
#include "constant_buffer.h"
#include "shader.h"


using Microsoft::WRL::ComPtr;

class Board
{
	VECTOR3 lt; 
	VECTOR3 rt;
	VECTOR3 lb;
	VECTOR3 rb;
	struct Vertex
	{
		VECTOR3 position;
		VECTOR2 texcoord;
		VECTOR4 color;
	};

	struct CBuffer
	{
		FLOAT4X4 worldViewProjection;
		VECTOR4 color;
	};

	ComPtr<ID3D11Buffer> buffer;
	std::unique_ptr<Shader> shader;
	std::unique_ptr<ConstantBuffer<CBuffer>> cb;
	ComPtr<ID3D11RasterizerState> rasterizer;
	ComPtr<ID3D11SamplerState> sampler;
	ComPtr<ID3D11DepthStencilState> depthStensilState;
	ComPtr<ID3D11ShaderResourceView> srv;
	D3D11_TEXTURE2D_DESC texture2dDesc;

public:
	Board(ID3D11Device* device, const wchar_t* filename, bool oblon = true); // 長方形かどうか
	void Render(ID3D11DeviceContext* context, const VECTOR3& position, const float& scale, 
		const FLOAT4X4&  view, const FLOAT4X4& projection, const VECTOR4& color);
};

