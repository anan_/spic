
#include "frame_rate.h"
#include <sstream>

//Frame rate is displayed from calculated
void CalculateFrameStats(HWND hwnd, High_Resolution_Timer* timer)
{
    // Code computes the average frames per second, and also the 
    // average time it takes to render one frame.  These stats 
    // are appended to the window caption bar.
    static int frames = 0;
    static float elapsed_time = 0.0f;

    frames++;

    // Compute averages over one second period.
    if ((timer->time_stamp() - elapsed_time) >= 1.0f)
    {
        float fps = static_cast<float>(frames); // fps = frameCnt / 1
        float mspf = 1000.0f / fps;
        std::ostringstream outs;
        outs.precision(6);
        outs << "FPS : " << fps << " / " << "Frame Time : " << mspf << " (ms)";
        SetWindowTextA(hwnd, outs.str().c_str());

        // Reset for next average.
        frames = 0;
        elapsed_time += 1.0f;
    }
}