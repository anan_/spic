
#include "scene_game.h"
#include "create_DX11.h"
using namespace GetCoreSystem;
#include "misc.h"
#include <string>
#include <sstream>
#include <iostream>

#include "font.h"
#include "random.h"
#include "load_model.h"
#include "character_manager.h"
#include "enemy_manager.h"
#include "shot_manager.h"
#include "judge.h"
#include "collision.h"
#include "input.h"
#include "scene.h"
#include "scene_manager.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

void SceneGame::CreateLight(ID3D11Device* device)
{
	Light::Init(device);
	lightBuffer = std::make_unique<ConstantBuffer<CbLight>>(device);
}

void SceneGame::Init(ID3D11Device* device)
{
	CreateLight(device);
	camera = std::make_shared<Camera>();
	renderer = std::make_unique<ModelRenderer>(device);

	//setumei_img = std::make_unique<Sprite>(device, L"Data/images/setumei.png");

	pCharacterManager.Create(device);
	pEnemyManager.Create(device);
	pShotManager.Create(device);

	stageMove = std::make_shared<Stage>();
	//field = std::make_unique<Obj3D>();
	//field->SetModelResource(pLoadModel.GetModelResource("Field"));
	//field->SetMoveAlg(stageMove.get());
	//field->SetScale(VECTOR3(100,100,100));
	//field->SetRotation(VECTOR3(0, 0, 0));
	//field->SetPosition(VECTOR3(0, -50.f, 0));
	//Collision::RegisterTerrain(field.get());

	kitchen = std::make_unique<Obj3D>();
	kitchen->SetModelResource(pLoadModel.GetModelResource("Kitchen"));
	kitchen->SetMoveAlg(stageMove.get());
	kitchen->SetScale(VECTOR3(1, 1, 1));
	kitchen->SetRotation(VECTOR3(0, 0, 0));
	kitchen->SetPosition(VECTOR3(0, 0, 0));


	hand = std::make_unique<Obj3D>();
	hand->SetModelResource(pLoadModel.GetModelResource("Hand"));
	hand->SetMoveAlg(stageMove.get());
	hand->SetScale(VECTOR3(1, 1, 1));
	hand->SetRotation(VECTOR3(0, 0, 0));
	hand->SetPosition(VECTOR3(0, 0, 0));
}

void SceneGame::Release()
{
	pCharacterManager.Destory();
	pEnemyManager.Destory();
	pShotManager.Destory();
}

void SceneGame::Update(ID3D11Device* device, float elapsedTime)
{
	camera->Update();
	//field->Update(elapsedTime);
	kitchen->Update(elapsedTime);
	hand->Update(elapsedTime);
	pCharacterManager.Update(elapsedTime);
	pEnemyManager.Update(elapsedTime);
	pShotManager.Update(elapsedTime);
	float XM_PI = 3.141592654f;
	static float lightAngle = XM_PI;
	Light::SetAmbient(VECTOR3(0.5f, 0.5f, 0.5f));
	//ライト方向
	LightDir.x = sinf(lightAngle);
	//でっけえライト
	LightDir.y = 0.0f;
	LightDir.z = cosf(lightAngle);
	static float angle = XM_PI / 4;
	//angle += elapsedTime;
	Light::SetDirLight(LightDir, VECTOR3(1.0f, 1.0f, 1.0f));

	// エディタ-
#ifdef USE_IMGUI
	ImGui::Begin("Editer");

	ImGuiTabBarFlags flag = ImGuiTabBarFlags_Reorderable; // タブをドラッグして並び替えできる
	flag |= ImGuiTabBarFlags_NoTabListPopupButton; // タブの一番左端にドロップダウンリストが表示される下向き三角形のクリックエリアを作成し、そこからタブを選択できるようになります。
	flag |= ImGuiTabBarFlags_AutoSelectNewTabs; // タブを新しく作成した時に自動でそのタブを選択状態にします。
	// flag |= ImGuiTabBarFlags_NoCloseWithMiddleMouseButton; // タブの中でマウス中央ボタンクリックすることでタブを閉じることができる機能を無効にします。
	// flag |= ImGuiTabBarFlags_NoTooltip; // タブ上にマウスオーバーした場合に表示されるタブ名のポップアップ表示を無効にします。
	// flag |= ImGuiTabBarFlags_FittingPolicyResizeDown; // タブがウィンドウ幅を超えてたくさんある場合にタブの幅を自動でリサイズしてフィットさせることができます。
	flag |= ImGuiTabBarFlags_FittingPolicyScroll;// タブの幅を自動でリサイズさせずに左右の矢印ボタンを右端に配置してそこからタブを順番に選択できるようにします。
	const char* names[5] = { "Player", "Camera", "Enemy", "Render", "Field" };// 同じタブ名を使いたい場合は、"##1" や "##2" のように区別させる必要があります
	static bool opend[5] = { true, true, true, true, true };

	static float fieldScale = 0;



	if (ImGui::BeginTabBar("TabBarID", flag))
	{
		for (int n = 0; n < IM_ARRAYSIZE(opend); n++)
		{
			// 第2引数の&opened[n]を省略すると閉じるボタン(X)が作成されません。
			if (opend[n] && ImGui::BeginTabItem(names[n]))
			{
				switch (n)
				{
				case 0: // Player

					break;

				case 1: // Camera
					ImGui::Text("Eye x:%.3f  y:%.3f  z %.3f", camera->GetEye().x, camera->GetEye().y, camera->GetEye().z);
					ImGui::Text("Focus x:%.3f  y:%.3f  z %.3f", camera->GetFocus().x, camera->GetFocus().y, camera->GetFocus().z);

					ImGui::Text("front x:%.3f  z %.3f", camera->GetFront().x, camera->GetFront().z);
					break;

				case 2: // Enemy
					if (ImGui::CollapsingHeader("EnemyAdd"))
					{
						//if (ImGui::TreeNode("Slime"))
						//{
						ImGui::BeginChild(ImGui::GetID((void*)0), ImVec2(250, 200), ImGuiWindowFlags_NoTitleBar);

						//static VECTOR3 pos;

						//ImGui::Text("EnemyNum:%02d", enemyNum);
						//ImGui::Text("Position");
						//ImGui::InputFloat("X", &pos.x, 0.01f, 1.0f, "%.1f");
						//ImGui::InputFloat("Y", &pos.y, 0.01f, 1.0f, "%.1f");
						//ImGui::InputFloat("Z", &pos.z, 0.01f, 1.0f, "%.1f");
						//if (ImGui::Button("Add") && enemyNum < SlimeMax)
						//{
						//	slimeMove[enemyNum] = std::make_shared<Slime>();
						//	slimeMove[enemyNum]->SetCamera(camera);
						//	pEnemyManager.Add(GetDevice(), pLoadModel.GetModelResource("Slime"),
						//		slimeMove[enemyNum].get(),
						//		VECTOR3(1, 1, 1), VECTOR3(0, 0, 0), pos);
						//	enemyNum++;
						//}
						ImGui::EndChild();

						//ImGui::TreePop();
						//}
					}

					//ImGui::Text("EnemyDead:%02d", deadCount);

					ImGui::BeginChild("ChildID", ImVec2(250, 260), true, flag);
					//for (auto& e : *pEnemyManager.GetList())
					//{

					//	ImGui::Text("X:%.2f", e.GetPosition().x);
					//	ImGui::Text("Y:%.2f", e.GetPosition().y);
					//	ImGui::Text("Z:%.2f", e.GetPosition().z);
					//}
					ImGui::EndChild();


					break;

				case 3: // Render
					break;

				case 4: // Field;

					ImGui::InputFloat("Scale", &fieldScale, 0.0f, 1.f, "%.1f");
					field->SetScale(VECTOR3(fieldScale, fieldScale, fieldScale));
					break;
				}

				// BeginTabItem() の終了処理
				ImGui::EndTabItem();
			}
		}

		// BeginTabBar() の終了処理
		ImGui::EndTabBar();
	}

	camera->SetFocus(VECTOR3(0,0,0));

	ImGui::End();
#endif
}

void SceneGame::Render(ID3D11DeviceContext* context, float elapsedTime)
{
	SetRender::SetBlender(BS_ALPHA);
	SetRender::SetDepthStencilState(DS_TRUE);
	SetRender::SetRasterizerState(RS_CULL_FRONT);

	lightBuffer->data.ambientColor = Light::Ambient;
	lightBuffer->data.lightDir = Light::LightDir;
	lightBuffer->data.lightColor = Light::DirLightColor;
	lightBuffer->data.eyePos.x = camera->GetEye().x;
	lightBuffer->data.eyePos.y = camera->GetEye().y;
	lightBuffer->data.eyePos.z = camera->GetEye().z;
	lightBuffer->data.eyePos.w = 1.0f;

	//memcpy(lightBuffer->data.PointLight, Light::PointLight, sizeof(POINTLIGHT)*Light::POINTMAX);
	//memcpy(lightBuffer->data.SpotLight, Light::SpotLight, sizeof(SPOTLIGHT)*Light::SPOTMAX);

	// モデルの描画
	{
		lightBuffer->Activate(context, 3);
		renderer->Begin(context, camera->GetViewProection());

		for (auto &it : *pCharacterManager.GetList())
		{
			renderer->Draw(context, it.GetModel());
		}
		//renderer->Draw(context, field->GetModel());
		renderer->Draw(context, kitchen->GetModel());
		renderer->Draw(context, hand->GetModel());
		//
		float acs = 0;

		for (auto& slime : *pEnemyManager.GetList())
		{
			// カメラZが反転している

			DirectX::XMVECTOR start = DirectX::XMLoadFloat3(&camera->GetEye());
			DirectX::XMVECTOR end = DirectX::XMLoadFloat3(&slime.GetPosition());

			//DirectX::XMVECTOR l = DirectX::XMVector3TransformCoord(end, iw);
			DirectX::XMVECTOR vec = DirectX::XMVectorSubtract(end, start);
			DirectX::XMVECTOR len = DirectX::XMVector3Length(vec);
			float fLen;
			DirectX::XMStoreFloat(&fLen, len);


			DirectX::XMVECTOR cameraFront = DirectX::XMLoadFloat3(&camera->GetFront());

			DirectX::XMVECTOR cameraFront2 = DirectX::XMLoadFloat3(&(camera->GetFocus() - camera->GetEye()));
			cameraFront2.m128_f32[1] = vec.m128_f32[1] = 0;

			cameraFront = DirectX::XMVector3Normalize(cameraFront2);

			vec = DirectX::XMVector3Normalize(vec);


			DirectX::XMVECTOR dot = DirectX::XMVector3Dot(vec, cameraFront);


			float fdot;
			DirectX::XMStoreFloat(&fdot, dot);

			acs = acos(fdot);

			if (acs <= 15.f * 0.01745f)
			{
				renderer->Draw(context, slime.GetModel());

			}
		}

		//renderer->Draw(context, playerBodyMove->GetCubeModel(), VECTOR4(1, 1, 1, 0.5f));

		renderer->End(context);

		pShotManager.Render(context, camera->GetViewProection());

		lightBuffer->DeActivate(context);
	}

}