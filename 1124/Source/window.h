#pragma once

constexpr char* WINDOW_TEXT = "DirectX11";
constexpr float SCREEN_WIDTH = 1280;
constexpr float SCREEN_HEIGHT = 920;

constexpr bool WINDOW_SCREEN = false;
constexpr bool FULL_SCREEN = false;
constexpr bool WINDOW_MODE = WINDOW_SCREEN;
