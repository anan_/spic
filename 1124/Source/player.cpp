
#include "player.h"
#include "load_model.h"
#include "sound_data.h"
#include "emulate_gamepad.h"
#include "shot_manager.h"
#include "collision.h"

#include "create_DX11.h"
using namespace GetCoreSystem;

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

PlayerBody::PlayerBody()
{
	SetAcceleration(9.0f);
	SetDeceleration(6.0f);
	SetMaxSpeed(50.0f);
}

void PlayerBody::Move(Obj3D* obj, float elapsedTime)
{
	// 移動
	const VECTOR2& PAD = EmulateGamePad::WASD();
	const VECTOR3& right = camera->GetRight();
	const VECTOR3& front = camera->GetFront();
	VECTOR3 force;
	force.x = (right.x * PAD.x) + (front.x * PAD.y);
	force.y = 0;
	force.z = (-right.z * PAD.x) + (-front.z * PAD.y);

	AddForce(force);
	PlayerBodyMove(obj, elapsedTime);

	//VECTOR3 outPos, position;
	//position = obj->GetPosition();
	//position.y += 2.f; force.y = position.y;
	//if (-1 != Collision::MoveCheck(position, force, &outPos))
	//{
	//	obj->SetPosition(outPos);
	//}

	SetCube(VECTOR3(1, 1, 1), obj->GetRotation(), obj->GetPosition());
}

PlayerGun::PlayerGun()
{
	SetAcceleration(0.0f);
	SetDeceleration(0.0f);
	SetMaxSpeed(0.0f);
	//shotSound = std::make_unique<Sound>("Data/sounds/cannon1.wav");
}

void PlayerGun::Move(Obj3D* obj, float elapsedTime)
{
	PlayerGunMove(obj, elapsedTime);
	// ショット
	VECTOR3 position = obj->GetPosition();
	static Key SPACE(VK_SPACE);
	static Key LBUTTON(VK_LBUTTON);
	static int s = 0;
	if (s >= shot.size() - 1)
	{
		s = 0;
	}
	if (LBUTTON.state(TRIGGER_MODE::RISINING_EDGE))
	{
		shot[s] = std::make_unique<PlayerShot>();
		VECTOR3 angle = obj->GetRotation();
		float dx = sinf(angle.y);
		float dz = cosf(angle.y);
		VECTOR3 plFront(dx, 0, dz);

		VECTOR3 setPos = VECTOR3(position.x, position.y + 7.25f, position.z);
		pShotManager.Add(GetDevice(), pLoadModel.GetModelResource("Shot"),
			shot[s].get(),
			VECTOR3(50.f, 50.f, 50.f), VECTOR3(0, 0, 0), setPos);

		shot[s]->AddForce(plFront);

		s++;
	}

	SetCube(VECTOR3(1, 1, 1), obj->GetRotation(), obj->GetPosition());
}