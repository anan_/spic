#pragma once

#include "obj3d_manager.h"

class CharacterManager : public Obj3DManager
{
    static CharacterManager* instance;

public:
    CharacterManager(ID3D11Device* device) : Obj3DManager(device){}
    ~CharacterManager() {}

    void Update(float elapsedTime);

public:
    static void Create(ID3D11Device* device)
    {
        if (instance != nullptr) return;
        instance = new CharacterManager(device);
    }
    static CharacterManager& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};

#define pCharacterManager CharacterManager::GetInstance()
