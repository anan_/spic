#pragma once

#include <string>
#include <vector>
#include "vector.h"
#include <wrl.h>

using Microsoft::WRL::ComPtr;

struct ModelData
{
	struct Node
	{
		std::string name;
		int parentIndex;
		VECTOR3 scale;
		VECTOR4 rotate;
		VECTOR3 translate;
	};

	struct Vertex
	{
		VECTOR3 position;
		VECTOR3 normal;
		VECTOR2 texcoord;
		VECTOR4 boneWeight;
		UINT4 boneIndex;
	};

	struct Subset
	{
		int materialIndex;
		int startIndex;
		int indexCount;
	};

    struct Face
    {
        VECTOR3 position[3];
        int materialIndex;
    };

	struct Mesh
	{
		std::vector<Vertex> vertices;
		std::vector<int> indices;
		std::vector<Subset> subsets;

		int nodeIndex;

		std::vector<int> nodeIndices;
        FLOAT4X4 globalTransform = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };
		std::vector<FLOAT4X4> inverseTransforms;

        std::vector<Face> faces;
	};

	struct Material
	{
		VECTOR4 color;
		std::string textureFileName;
	};

	struct NodeKeyData
	{
		VECTOR3 scale;
		VECTOR4 rotate;
		VECTOR3 translate;
	};

	struct KeyFrame
	{
		float seconds;
		std::vector<NodeKeyData> nodeKeys;
	};
	struct Animation
	{
		float secondsLength;
		std::vector<KeyFrame> keyFrames;
	};

	std::vector<Node> nodes;

	std::vector<Mesh> meshes;
	std::vector<Material> materials;

	std::vector<Animation> animations;

	char dirName[256];
};