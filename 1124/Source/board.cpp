
#include "board.h"
#include "create_DX11.h"
#include "resource_manager.h"


Board::Board(ID3D11Device* device, const wchar_t* filename, bool oblon)
{
	HRESULT hr = S_OK;
	if (!oblon)
	{
		lt = VECTOR3(-0.5f, +0.5f, 0.0f);
		rt = VECTOR3(+0.5f, +0.5f, 0.0f);
		lb = VECTOR3(-0.5f, -0.5f, 0.0f);
		rb= VECTOR3(+0.5f, -0.5f, 0.0f);
	}
	else
	{
		lt = VECTOR3(-1.0f, +0.5f, 0.0f);
		rt = VECTOR3(+1.0f, +0.5f, 0.0f);
		lb = VECTOR3(-1.0f, -0.5f, 0.0f);
		rb = VECTOR3(+1.0f, -0.5f, 0.0f);
	}

	Vertex vertices[4] =
	{
		{ lt, VECTOR2(1, 0) },
		{ rt, VECTOR2(0, 0) },
		{ lb, VECTOR2(1, 1) },
		{ rb, VECTOR2(0, 1) },
	};

	{
		D3D11_BUFFER_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		desc.Usage = D3D11_USAGE_DYNAMIC;
		desc.ByteWidth = sizeof(Vertex) * 4;
		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		D3D11_SUBRESOURCE_DATA data;
		ZeroMemory(&data, sizeof(data));
		data.pSysMem = vertices;
		hr = device->CreateBuffer(&desc, &data, buffer.GetAddressOf());
		if (FAILED(hr))
		{
			assert(!"VertexBuffer Board");
		}
	}

	shader = std::make_unique<Shader>();
	shader->CreateBoard(device);

	cb = std::make_unique<ConstantBuffer<Board::CBuffer>>(device);

	{
		D3D11_RASTERIZER_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		desc.FillMode = D3D11_FILL_SOLID;
		desc.CullMode = D3D11_CULL_NONE;
		desc.FrontCounterClockwise = FALSE;
		desc.DepthBias = 0;
		desc.DepthBiasClamp = 0;
		desc.SlopeScaledDepthBias = 0;
		desc.DepthClipEnable = FALSE;
		desc.ScissorEnable = FALSE;
		desc.MultisampleEnable = FALSE;
		desc.AntialiasedLineEnable = FALSE;
		hr = device->CreateRasterizerState(&desc, rasterizer.GetAddressOf());
		if (FAILED(hr))
		{
			assert(!"RasterizerState Board");
		}
	}

	{
		D3D11_SAMPLER_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		desc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
		desc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
		desc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
		desc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		desc.MinLOD = -FLT_MAX;
		desc.MaxLOD = FLT_MAX;
		desc.MaxAnisotropy = 16;
		desc.BorderColor[0] = 1.0f;
		desc.BorderColor[1] = 1.0f;
		desc.BorderColor[2] = 1.0f;
		desc.BorderColor[3] = 1.0f;
		hr = device->CreateSamplerState(&desc, sampler.GetAddressOf());
		if (FAILED(hr))
		{
			assert(!"SamplerState Board");
		}
	}

	{
		D3D11_DEPTH_STENCIL_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		desc.DepthEnable = FALSE;
		desc.StencilEnable = FALSE;
		desc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
		desc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
		desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		hr = device->CreateDepthStencilState(&desc, depthStensilState.GetAddressOf());
		if (FAILED(hr))
		{
			assert(!"DepthStencilState Board");
		}
	}

	ResourceManager::LoadShaderResourceView(device, filename, srv.GetAddressOf(), &texture2dDesc);
}

void Board::Render(ID3D11DeviceContext* context, const VECTOR3& position, const float& scale,
	const FLOAT4X4& view, const FLOAT4X4& projection, const VECTOR4& color)
{
	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	context->IASetVertexBuffers(0, 1, buffer.GetAddressOf(), &stride, &offset);
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	shader->Activate(context);

	context->RSSetState(rasterizer.Get());

	context->PSSetShaderResources(0, 1, srv.GetAddressOf());
	context->PSSetSamplers(0, 1, sampler.GetAddressOf());

	context->OMSetDepthStencilState(depthStensilState.Get(), 0);

	{
		DirectX::XMMATRIX S, T, W, WVP;
		{
			S = DirectX::XMMatrixScaling(scale, scale, scale);
			T = DirectX::XMMatrixTranslation(position.x, position.y, position.z);
			W = S * T;
		}
		DirectX::XMMATRIX V, P, invView;
		{
			V = DirectX::XMLoadFloat4x4(&view);
			P = DirectX::XMLoadFloat4x4(&projection);
			FLOAT4X4 temp = view;
			// 位置情報削除
			temp._41 = 0.0f; temp._42 = 0.0f;
			temp._43 = 0.0f; temp._44 = 1.0f;
			invView = DirectX::XMLoadFloat4x4(&temp);
			invView = DirectX::XMMatrixInverse(nullptr, invView);
			WVP = invView * W * V * P;
			FLOAT4X4 wvp;
			DirectX::XMStoreFloat4x4(&wvp, WVP);

			cb->data.worldViewProjection = wvp;
			cb->data.color = color;
			cb->Activate(context, 1);
		}
	}

	context->Draw(4, 0);
}

