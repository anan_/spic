#pragma once

#include <d3d11.h>
#include <wrl.h>

using Microsoft::WRL::ComPtr;

class BlendState
{
    int now_blender = 0;
    ID3D11DeviceContext* keep_context = nullptr;
public:
    static const int BLEND_TYPE = 9;
    
    ComPtr<ID3D11BlendState> blend_state[BLEND_TYPE];

    BlendState() { }
    virtual ~BlendState() { }

    HRESULT CreateBlendState(ID3D11Device* device, ID3D11DeviceContext* context);
    void SetBlendState(const int blendMode, ID3D11DeviceContext* context = nullptr);
};
enum BLEND_LAVEL { BS_NONE, BS_ALPHA, BS_ADD, BS_SUBTRACT, BS_REPLACE, BS_MULTIPLY, BS_LIGHTEN, BS_DARKEN, BS_SCREEN };
