#pragma once

#include "model.h"
#include "model_data.h"
#include "model_resource.h"
#include "fbx_loader.h"
#include <map>

class LoadModel
{
    static LoadModel* instance;
    static std::map<std::string, std::shared_ptr<ModelResource>> modelDatas;

    std::shared_ptr<Model> model;
public:
    LoadModel() { this->instance; }
    void Load(ID3D11Device* device, const char* filename);
    //std::shared_ptr<Model> Load(ID3D11Device* device, const char* filename, const char* modelTag);
    std::shared_ptr<Model> GetModel() { return model; }

    // 
    void Load(ID3D11Device* device, const char* filename, const char* modelTag);
    std::shared_ptr<ModelResource>& GetModelResource(const char* modelTag);

    static LoadModel& GetInstance()
    {
        return *instance;
    }
};

#define pLoadModel LoadModel::GetInstance()
