
#include "create_DX11.h"
#include "window.h"
#include "scene_manager.h"

int APIENTRY wWinMain(HINSTANCE instance, HINSTANCE prevInstance, LPWSTR cmdLine, int cmdShow)
{
    Create_DX11 create_dx11;
    create_dx11.Init(WINDOW_TEXT, static_cast<int>(SCREEN_WIDTH), static_cast<int>(SCREEN_HEIGHT), FULL_SCREEN, instance);
    create_dx11.SetScreenColor(0.2f, 0.2f, 0.2f);

    SceneManager::Create(instance, &create_dx11, GetCoreSystem::GetDevice());
    SceneManager::GetInstance().Execute(instance, GetCoreSystem::GetContext(), GetCoreSystem::GetDevice());
    SceneManager::Destory();

    create_dx11.Release();
    return 0;
}