// UNIT.02
#pragma once

#include <memory>
#include <d3d11.h>
#include <wrl.h>
#include "model_data.h"

class ModelResource
{
public:
	ModelResource(ID3D11Device* device, std::unique_ptr<ModelData> data);
	~ModelResource() {}

	struct Material
	{
		VECTOR4 color = { 0.8f, 0.8f, 0.8f, 1.0f };
		ComPtr<ID3D11ShaderResourceView> shaderResourceView;
	};

	struct Subset
	{
		u_int startIndex = 0;
		u_int indexCount = 0;
		Material* material;
	};

	struct Mesh
	{
		ComPtr<ID3D11Buffer> vertexBuffer;
		ComPtr<ID3D11Buffer> indexBuffer;
		std::vector<Subset> subsets;

		int nodeIndex;
		std::vector<int> nodeIndices;
		std::vector<FLOAT4X4*> inverseTransforms;
	};

	const std::vector<Mesh>& GetMeshes() const { return meshes; }
	const std::vector<ModelData::Node>& GetNodes() const { return data->nodes; }
	const std::vector<ModelData::Animation>& GetAnimations() const { return data->animations; }
	//const std::vector<ModelData::Face>& GetFaces() const { return data->meshes.begin()->faces; }
    const std::vector<ModelData::Mesh>& GetMesh() const { return data->meshes; }

private:
	std::unique_ptr<ModelData> data;
	std::vector<Material> materials;
	std::vector<Mesh> meshes;
};
