#pragma once

#include "input.h"
#include "vector.h"

class EmulateGamePad
{
private:

public:
    static VECTOR2 LStick(/*TRIGGER_MODE triggerMode*/);
    static VECTOR2 WASD();
};
