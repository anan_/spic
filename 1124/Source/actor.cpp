#include "actor.h"
#include "load_model.h"

#include "input.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

Actor::Actor() : pram()
{
	pram.maxSpeed = 3.0f;
	pram.velocity = VECTOR3(0, 0, 0);
	pram.acceleration = 0.1f;
	pram.deceleration = 0.1f;
	pram.turnSpeed = DirectX::XMConvertToRadians(360);

	cube = std::make_unique<Obj3D>();
	//cube->SetModelResource(pLoadModel.GetModelResource("Cube"));
}

void Actor::SetCube(const VECTOR3& scale, const VECTOR3& rotation, const VECTOR3& position)
{
	cube->SetScale(scale);
	cube->SetRotation(rotation);
	cube->SetPosition(position);

	cube->Update(0.f);
}

void Actor::MainMove(Obj3D* obj, float elapsedTime)
{

	//ベクトルの長さを計算
	float speed = Vec3Length(pram.velocity);

	//減速率よりも高かったら
	if (speed > pram.deceleration)
	{
		//移動 (単位ベクトルで向きを作る)
		float vx = pram.velocity.x / speed;
		float vy = pram.velocity.y / speed;
		float vz = pram.velocity.z / speed;
		//大きくなりすぎたら止める
		if (speed > pram.maxSpeed)
		{
			pram.velocity.x = vx * pram.maxSpeed;
			pram.velocity.y = vy * pram.maxSpeed;
			pram.velocity.z = vz * pram.maxSpeed;
		}
		else
		{
			//滑らかにするために減速
			pram.velocity.x -= vx * pram.deceleration;
			pram.velocity.y -= vy * pram.deceleration;
			pram.velocity.z -= vz * pram.deceleration;
		}
		// ただの移動
		VECTOR3 position = obj->GetPosition();

		position.x += pram.velocity.x * elapsedTime;
		position.y += pram.velocity.y * elapsedTime;
		position.z += pram.velocity.z * elapsedTime;
		obj->SetPosition(position);
		//回転
		VECTOR3 angle = obj->GetRotation();

		//プレイヤーのベクトルを算出
		float dx = sinf(angle.y);
		float dz = cosf(angle.y);

		//内積
		//ベクトルが狭くなっていくごとに内積の値が小さくなっていく
		float dot = (vx * dx) + (vz * dz);
		float rot = (1.0f - dot);
		//1fにどれだけ回転するか (elapsedTime)
		float limit = pram.turnSpeed * elapsedTime;
		if (rot > limit)
		{
			rot = limit;
		}
		//外積　（残り何度)
		float cross = (vx * dz) - (vz * dx);
		if (cross > 0.0f)
		{
			angle.y += rot;
		}
		else
		{
			angle.y -= rot;
		}
		obj->SetRotation(angle);
	}
	else
	{
		//減速率よりも低くなったら
		pram.velocity.x = 0.0f;
		pram.velocity.y = 0.0f;
		pram.velocity.z = 0.0f;
	}
}

void Actor::PlayerBodyMove(Obj3D * obj, float elapsedTime)
{
	static Key SHIFT(VK_LSHIFT);
	if (SHIFT.state(TRIGGER_MODE::NONE))
	{ 
		this->SetMaxSpeed(150.0f);
	}
	else
	{
		this->SetMaxSpeed(50.0f);
	}

	MainMove(obj, elapsedTime);
	VECTOR3 pos = obj->GetPosition();
	if (pos.x >= 830)
	{
		pos.x = 830;
	}
	if (pos.x <= -750)
	{
		pos.x = -750;
	}

	if (pos.z >= 380)
	{
		pos.z = 380;
	}
	if (pos.z <= -1440)
	{
		pos.z = -1440;
	}
	obj->SetPosition(pos);
}

void Actor::PlayerGunMove(Obj3D * obj, float elapsedTime)
{
	//回転
	VECTOR3 angle = obj->GetRotation();

	POINT cursor;
	::GetCursorPos(&cursor);

	oldCursor = newCursor;
	newCursor = VECTOR2(static_cast<float>(cursor.x), static_cast<float>(cursor.y));

	//float moveX = (oldCursor.x - newCursor.x) * 0.02f;
	//float moveY = (oldCursor.y - newCursor.y) * 0.02f;

	//static float rotateY = 0.f;
	//rotateY -= moveX * 0.1f;

	//angle.y = DirectX::XMConvertToRadians(rotateY);
	//float sy = ::sinf(rotateY);
	//float cy = ::cosf(rotateY);

	float mouse_x = (newCursor.x) * 0.00655f;
	//float mouse_y = (newCursor.y - 200)  * 0.001f;

	//static Key L(VK_LEFT);
	//static Key R(VK_RIGHT);
	//static float a = DirectX::XMConvertToRadians(0.f);
	//if (L.state(TRIGGER_MODE::NONE))
	//{
	//	a += DirectX::XMConvertToRadians(1.f);
	//}
	//if (R.state(TRIGGER_MODE::NONE))
	//{
	//	a -= DirectX::XMConvertToRadians(1.f);
	//}
	//angle.y = a;

	angle.y = mouse_x;
	//angle.x = mouce_y;

	obj->SetRotation(angle);
	if (cursor.x <= 0)
	{
		::SetCursorPos(1919, cursor.y);
	}
	if (cursor.x >= 1919)
	{
		::SetCursorPos(0, cursor.y);
	}
}

void Actor::PlayerShotMove(Obj3D * obj, float elapsedTime)
{
	MainMove(obj, elapsedTime);
}

void Actor::EnemyMove(Obj3D* obj, float elapsedTime)
{
	MainMove(obj, elapsedTime);
}