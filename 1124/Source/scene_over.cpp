
#include "scene_over.h"
#include "scene_manager.h"
#include "input.h"
void SceneOver::Init(ID3D11Device* device)
{
	over_ping = std::make_unique<Sprite>(device, L"Data/images/over.png");
}

void SceneOver::Release()
{
}

void SceneOver::Update(ID3D11Device* device, float elapsedTime)
{
	static Key SPACE(VK_RETURN);
	if (SPACE.state(TRIGGER_MODE::RISINING_EDGE))
	{
		SceneManager::GetInstance().ChangeScene(device, ToSTRING(TITLE));
	}
}

void SceneOver::Render(ID3D11DeviceContext* context, float elapsedTime)
{
	over_ping->Render(context,
		VECTOR2(0, 0), VECTOR2(1920, 1080),
		VECTOR2(0, 0), VECTOR2(1920, 1080),
		1.0f);

}
