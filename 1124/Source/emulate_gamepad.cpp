
#include "emulate_gamepad.h"

VECTOR2 EmulateGamePad::LStick(/*TRIGGER_MODE triggerMode*/)
{
    float stickX = 0.0f;
    float stickY = 0.0f;
    //static Key RIGHT(VK_RIGHT);
    //static Key LEFT(VK_LEFT);
    //static Key UP(VK_UP);
    //static Key DOWN(VK_DOWN);

    //LEFT
    if (GetAsyncKeyState(VK_LEFT) & 0x8000)
        stickX = -1.0f;
    //RIGHT
    if (GetAsyncKeyState(VK_RIGHT) & 0x8000)
        stickX = 1.0f;
    //UP
    if (GetAsyncKeyState(VK_UP) & 0x8000)
        stickY = 1.0f;
    //DOWN
    if (GetAsyncKeyState(VK_DOWN) & 0x8000)
        stickY = -1.0f;

    float power = sqrtf(stickX*stickX + stickY*stickY);
    if (stickX >= 1.0f || stickX <= -1.0f || 
        stickY >= 1.0f || stickY <= -1.0f)
    {
        stickX /= power;
        stickY /= power;

        return VECTOR2(stickX, stickY);
    }

    return  VECTOR2(stickX, stickY);
}

VECTOR2 EmulateGamePad::WASD()
{
    float stickX = 0.0f;
    float stickY = 0.0f;

    static Key W('W');
    static Key A('A');
    static Key S('S');
    static Key D('D');

    //  if (A.state(TRIGGER_MODE::NONE))// stickX = 

    //LEFT
    if (A.state(TRIGGER_MODE::NONE))
        stickX = -1.0f;
    //RIGHT
    if (D.state(TRIGGER_MODE::NONE))
        stickX = 1.0f;
    //UP
    if (W.state(TRIGGER_MODE::NONE))
        stickY = -1.0f;
    //DOWN
    if (S.state(TRIGGER_MODE::NONE))
        stickY = 1.0f;

    float power = sqrtf(stickX*stickX + stickY*stickY);
    if (stickX >= 1.0f || stickX <= -1.0f ||
        stickY >= 1.0f || stickY <= -1.0f)
    {
        stickX /= power;
        stickY /= power;

        return VECTOR2(stickX, stickY);
    }

    return  VECTOR2(stickX, stickY);
}
