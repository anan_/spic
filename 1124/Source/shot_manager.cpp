
#include "shot_manager.h"

ShotManager* ShotManager::instance = nullptr;

void ShotManager::Update(float elapsedTime)
{
    Obj3DManager::Update(elapsedTime);
}

void ShotManager::Render(ID3D11DeviceContext* context, const FLOAT4X4& viewProjection)
{
    Obj3DManager::Render(context, viewProjection);
}
