#pragma once

#include <d3d11.h>
#include "vector.h"
#include "resource_manager.h"
#include "shader.h"
#include "texture.h"
#include <memory>

class Sprite
{
//private:
//    ComPtr<ID3D11Buffer> vertex_buffer;
//    std::unique_ptr<Texture> texture;
//    std::unique_ptr<Shader> shader;
//    ComPtr<ID3D11DepthStencilState> depth_stencil_state;
//
//    struct Vertex
//    {
//        VECTOR3 position;
//        VECTOR3 normal;
//        VECTOR2 texcoord;
//        VECTOR4 color;
//    };
//
//public:
//    Sprite(ID3D11Device* device);
//    Sprite(ID3D11Device* device, const wchar_t* filename);
//    virtual ~Sprite() { };
//
//    void Render(ID3D11DeviceContext* context,
//        const float dx, const float dy, const float dw, const float dh,
//        const float sx, const float sy, const float sw, const float sh,
//        const float alpha);
//    void Render(ID3D11DeviceContext* context,
//        const VECTOR2& pos, const VECTOR2& size,
//        const VECTOR2& texPos, const VECTOR2& texSize,
//        const float alpha);
//
//    ID3D11RenderTargetView* GetRenderTarget() { return texture->GetRenderTarget(); }
//    void SetTexture(ID3D11DeviceContext* context, u_int slot);

private:
    //        ComPtr<>
    ComPtr<ID3D11VertexShader> VS;
    ComPtr<ID3D11PixelShader> PS;
    ComPtr<ID3D11InputLayout> layout;

    std::unique_ptr<Shader> shader;

    ComPtr<ID3D11Buffer> vertexBuffer;

    ComPtr<ID3D11ShaderResourceView> SRV;
    D3D11_TEXTURE2D_DESC texture2d;
    ComPtr<ID3D11SamplerState> samplerState;

    ComPtr<ID3D11RasterizerState> rasterizer;
    ComPtr<ID3D11DepthStencilState> depthStencilState;

    struct Vertex
    {
        VECTOR3 position;
        VECTOR2 texcoord;
        VECTOR4 color;
    };

public:
    Sprite(ID3D11Device* device, const wchar_t* filename);
    virtual ~Sprite() { };

    void Render(ID3D11DeviceContext* context,
        const VECTOR2& pos, const VECTOR2& size,
        const VECTOR2& texPos, const VECTOR2& texSize,
        const float alpha);
};

class SpriteBatch
{
protected:
	struct Vertex
	{
		VECTOR3 position;
		VECTOR2 texcoord;
	};

	struct Instance
	{
		FLOAT4X4 ndcTransform;
		VECTOR4 texcoordTransform;
		VECTOR4 color;
	};

	u_int maxInstance;

	std::unique_ptr<Shader> shader;

	ComPtr<ID3D11Buffer> vertexBuffer;

	std::unique_ptr<Texture> texture;

	ComPtr<ID3D11ShaderResourceView> SRV;
	D3D11_TEXTURE2D_DESC texture2d;
	ComPtr<ID3D11SamplerState> samplerState;

	ComPtr<ID3D11Buffer> instanceBuffer;
	Instance* instances;
	u_int reserveNum;

	void Init(ID3D11Device* device, const wchar_t* filename, UINT instnce);

public:
	SpriteBatch(ID3D11Device* device, const wchar_t* filename, UINT instnce)
	{
		shader = std::make_unique<Shader>();
		shader->CreateSpriteBatch(device);

		Init(device, filename, instnce);
	}

	void Begin(ID3D11DeviceContext *context);
	void RenderCenter(float dx, float dy, float dw, float dh,
		float sx, float sy, float sw, float sh,
		float cx, float cy,
		float angle,
		float r, float g, float b, float a,
		bool reverse);
	void RenderCenter(const VECTOR2& position, const VECTOR2& size,
		const VECTOR2& texPosition, const VECTOR2& texSize,
		const VECTOR2& center, float angle,
		const VECTOR4& color,
		bool reverse);
	void RenderLeft(float dx, float dy, float dw, float dh,
		float sx, float sy, float sw, float sh,
		float angle,
		float r, float g, float b, float a,
		bool reverse);
	void RenderLeft(const VECTOR2& position, const VECTOR2& size,
		const VECTOR2& texPosition, const VECTOR2& texSize,
		float angle,
		const VECTOR4& color, 
		bool reverse);
	void End(ID3D11DeviceContext *context);

	void Text(std::string str,
		float x, float y, float w, float h,
		float r, float g, float b, float a);
	void Text(std::string str,
		const VECTOR2& position, const VECTOR2& size,
		const VECTOR4& color);
};

//class Text : public SpriteBatch
//{
//private:
//	static const u_int maxInstance = 256;
//	u_int imageFontSizeW;
//	u_int imageFontSizeH;
//	u_int numOf1Line;
//
//public:
//	Text() : SpriteBatch(maxInstance) {}
//	Text(ID3D11Device* device, const wchar_t* filename,
//		u_int imageFontSizeW, u_int imageFontSizeH);
//	virtual ~Text() { };
//
//	bool Init(ID3D11Device* device, const wchar_t* filename,
//		u_int imageFontSizeW, u_int imageFontSizeH = 0)
//	{
//		shader = std::make_unique<Shader>();
//		shader->CreateSpriteBatch(device);
//
//		Init(device, filename, maxInstance);
//
//		this->imageFontSizeW = imageFontSizeW;
//		this->imageFontSizeH = imageFontSizeH;
//		if (this->imageFontSizeH <= 0) this->imageFontSizeH = this->imageFontSizeW;
//		numOf1Line = texture->GetWidth() / imageFontSizeW;
//
//		return true;
//	}
//
//	bool Set(const char* str, float x, float y, float dispSizeW = 0, float dispSizeH = 0);
//
//};