
#include "load_model.h"

LoadModel* LoadModel::instance = nullptr;
std::map<std::string, std::shared_ptr<ModelResource>> LoadModel::modelDatas;

void LoadModel::Load(ID3D11Device* device, const char* filename)
{
    std::unique_ptr<ModelData> data;
    std::shared_ptr<ModelResource> resource;

    auto it = modelDatas.find(filename);
    if (it != modelDatas.end())
    {
        resource = it->second;
    }
    else
    {
        data = std::make_unique<ModelData>();
        FbxLoader loader;
        loader.Load(filename, *data);

        resource = std::make_shared<ModelResource>(device, std::move(data));
        modelDatas.insert(std::make_pair(filename, resource));
    }
    model = std::make_shared<Model>(resource);
}


void LoadModel::Load(ID3D11Device* device, const char* filename, const char* modelTag)
{
    std::unique_ptr<ModelData> data = nullptr;
    std::shared_ptr<ModelResource> resource = nullptr;

    auto it = modelDatas.find(modelTag);
    if (it != modelDatas.end())
    {
        //resource = it->second;
        return;
    }
    else
    {
        data = std::make_unique<ModelData>();
        FbxLoader loader;
        loader.Load(filename, *data);

        resource = std::make_shared<ModelResource>(device, std::move(data));
        modelDatas.insert(std::make_pair(modelTag, resource));
        return;
    }
}

std::shared_ptr<ModelResource>& LoadModel::GetModelResource(const char* modelTag)
{
    auto it = modelDatas.find(modelTag);
    return it->second;
    //if (it != modelDatas.end())
    //{
        //return it->second;
    //}
    //else
    //{
        //assert(!"Not Found ModelResource!!");
    //}
}

//std::shared_ptr<Model> LoadModel::Load(ID3D11Device* device, const char* filename, const char* modelTag)
//{
//    std::unique_ptr<ModelData> data;
//    std::shared_ptr<ModelResource> resource;
//
//    auto it = modelDatas.find(modelTag);
//    if (it != modelDatas.end())
//    {
//        resource = it->second;
//    }
//    else
//    {
//        data = std::make_unique<ModelData>();
//        FbxLoader loader;
//        loader.Load(filename, *data);
//
//        resource = std::make_shared<ModelResource>(device, std::move(data));
//        modelDatas.insert(std::make_pair(modelTag, resource));
//    }
//    std::shared_ptr<Model> mo;
//    mo = std::make_shared<Model>(resource);
//}
