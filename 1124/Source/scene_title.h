
#pragma once
#include "scene.h"
#include <memory>

#include "vector.h"
#include "camera.h"
#include "constant_buffer.h"

#include "model.h"
#include "model_renderer.h"

#include "obj3d.h"
#include "obj3d_manager.h"

#include "player.h"
#include "slime.h"
#include "stage.h"
#include "sprite.h"
#include "light.h"

class SceneTitle : public Scene
{
private:
	std::shared_ptr<Camera> camera;

	std::unique_ptr<ModelRenderer> renderer;

	std::unique_ptr<Obj3D> player;
	std::unique_ptr<Obj3D> field;

	//std::unique_ptr<Obj3D> slime;
	std::unique_ptr<Sprite> title_img;
	std::shared_ptr<PlayerGun> playerGunMove;
	std::shared_ptr<PlayerBody> playerBodyMove;
	std::shared_ptr<Stage> stageMove;
	std::unique_ptr<ErasePlayerShot> erasePlayerShot;

	struct CbLight
	{
		VECTOR4 lightColor;
		VECTOR4 lightDir;
		VECTOR4 ambientColor;
		VECTOR4 eyePos;

		POINTLIGHT PointLight[Light::POINTMAX];
		SPOTLIGHT SpotLight[Light::SPOTMAX];
	};
	std::unique_ptr<ConstantBuffer<CbLight>> lightBuffer;
	VECTOR3 light;
	// ���C�g
	VECTOR3 LightDir;

public:
	void Init(ID3D11Device* device);
	void Release();
	void CreateLight(ID3D11Device* device);
	void Update(ID3D11Device* device, float elapsedTime);
	void Render(ID3D11DeviceContext* context, float elapsedTime);

};