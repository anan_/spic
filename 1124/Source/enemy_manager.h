#pragma once

#include "obj3d_manager.h"

class EnemyManager : public Obj3DManager
{
    static EnemyManager* instance;

public:
    EnemyManager(ID3D11Device* device) : Obj3DManager(device) {}
    ~EnemyManager() {}

    void Update(float elapsedTime);
    void Render(ID3D11DeviceContext* context, const FLOAT4X4& viewProjection);

public:
    static void Create(ID3D11Device* device)
    {
        if (instance != nullptr) return;
        instance = new EnemyManager(device);
    }
    static EnemyManager& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};

#define pEnemyManager EnemyManager::GetInstance()