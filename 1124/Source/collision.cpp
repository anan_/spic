
#include "collision.h"

Obj3D* Collision::terrain = nullptr;

bool Collision::isSharpAngle(const VECTOR3& p1, const VECTOR3& p2, const VECTOR3& p3)
{
	VECTOR3 l1 = CreateVec3(p2, p3);
	VECTOR3 p1p2 = CreateVec3(p1, p2);
	VECTOR3 p1p3 = CreateVec3(p1, p3);
	Vec3Normalize(&l1, l1);
	Vec3Normalize(&p1p2, p1p2);
	Vec3Normalize(&p1p3, p1p3);
	
	float dot1 = Vec3Dot(l1, p1p2);
	float dot2 = Vec3Dot(l1, p1p3);

	if (dot1 > 0 && dot2 < 0) return true;

	return false;
}

float Collision::CalculatePointLineDist(const VECTOR3& point,
	const VECTOR3& l1Start, const VECTOR3& l1End,
	float* length, VECTOR3* vec)
{
	VECTOR3 l1 = CreateVec3(l1Start, l1End);
	float len = Vec3DistVecter(l1, l1);

	if (len > 0.f)
	{
		float dot = Vec3Dot(l1, (CreateVec3(l1, point)));
		*length = dot / len;
	}
	*vec = l1Start + l1;

	return Vec3Length(CreateVec3(point, *vec));
}

float Collision::CalculatePointSegmentDist(const VECTOR3& point,
	const VECTOR3& l1Start, const VECTOR3& l1End,
	float* length, VECTOR3* vec)
{
	VECTOR3 l1 = CreateVec3(l1Start, l1End);

	float len = CalculatePointLineDist(point,
		l1Start, l1End, length, vec);

	// 始点の外側
	if (!isSharpAngle(point, l1Start, l1End))
	{
		*vec = l1Start;
		return Vec3Length(CreateVec3(point, l1Start));
	}
	// 終点の外側
	else if (!isSharpAngle(point, l1End, l1Start))
	{
		*vec = l1End;
		return Vec3Length(CreateVec3(point, l1End));
	}

	return true;
}

bool Collision::HitSphere(const VECTOR3& p1, float r1, const VECTOR3& p2, float r2)
{
    //	半径の合算の2乗
    const float R2 = (r1 + r2) * (r1 + r2);

    //	中心同士の距離の2乗
    VECTOR3 vec = p1 - p2;
    float L2 = Vec3Dot(vec, vec);

    //	衝突判定
    if (L2 < R2) return	true;

    return false;
}

bool Collision::HitSphere(const Sphere& enemy, const Sphere& player)
{
	float pScaler = player.scale;
	float eScaler = enemy.scale;

	const float r = (pScaler * player.radius + eScaler * enemy.radius) *
		(pScaler * player.radius + eScaler * enemy.radius);

	VECTOR3 vec;
	vec = player.position - enemy.position;

	const float length = (vec.x * vec.x) + (vec.y * vec.y) + (vec.z * vec.z);

	if (length < r)
	{
		return true;
	}
	return false;
}

bool Collision::HitCapsule(const Capsule& enemy, const Sphere& player)
{
	VECTOR3 eVector = enemy.endPos - enemy.startPos;
	float eScaler = Vec3Length(eVector);
	Vec3Normalize(&eVector, eVector);

	VECTOR3 atkVector = enemy.startPos - player.position;
	float atkScaler = Vec3Length(atkVector);
	Vec3Normalize(&atkVector, atkVector);

	float dot = Vec3Dot(eVector, atkVector);

	//----------------------------------------------------------------
	//　　内積の範囲
	//	　http://nomoreretake.net/2015/12/01/what_is_dotprocudt_math/
	//----------------------------------------------------------------

	//内積が0以下なら球の当たり判定
	if (dot <= 0) 
	{
		Sphere sEnemy;
		sEnemy.position = enemy.startPos;
		sEnemy.radius = enemy.radius;
		sEnemy.scale = enemy.scale;

		if (HitSphere(player, sEnemy))
		{
			return true;
		}
		else
		{
			sEnemy.position = enemy.endPos;
			if (HitSphere(player, sEnemy))
			{
				return true;
			}
		}
	}
	else
	{
		//射影ベクトルの式のベクトルを掛ける前が共通してるので
		float pro = (dot / eScaler * eScaler);
		VECTOR3 projection;
		// 射影ベクトル
		projection = pro * eVector;

		// カプセルのスタートポジションから射影ベクトル分移動させたポジション
		VECTOR3 position;
		position = projection + enemy.startPos;

		// プレイヤーの攻撃判定ポジションと射影ポジション間のベクトル
		VECTOR3 vector;
		vector = player.position - position;

		float scaler = Vec3Length(vector);

		float r = enemy.radius * enemy.scale + player.radius * player.scale;

		if (scaler <= r)
		{
			return true;
		}
	}

	return false;
}

float Collision::CalculateLineLine(const VECTOR3& l1Start, const VECTOR3& l1End,
	const VECTOR3& l2Start, const VECTOR3& l2End,
	VECTOR3* pointl1, VECTOR3* pointl2)
{
	//// A=B C=Dのときは計算できない
	////if (Vec3DistVecter(l1Start, l1End) == 0 || Vec3DistVecter(l2Start, l2End) == 0) return 0.f;

	//VECTOR3 l1 = CreateVec3(l1Start, l1End);
	//VECTOR3 l2 = CreateVec3(l2Start, l2End);

	//VECTOR3 l1Dir, l2Dir;
	//Vec3Normalize(&l1Dir, l1);
	//Vec3Normalize(&l2Dir, l2);

	//float dot = Vec3Dot(l1Dir, l2Dir);
	//float work = 1 - dot * dot;

	//// 平行
	//if (work == 0)
	//{
	//	float len = CalculatePointLineDist(l1Start, l2Start, l2End, nullptr, pointl2);
	//	*pointl1 = l1Start;
	//	return len;
	//}

	//// 2直線はねじれ関係
	//dot = Vec3Dot(l1, l2);
	//float l1Len = Vec3DistVecter(l1, l1);
	//float l2Len = Vec3DistVecter(l2, l2);
	//VECTOR3 vec = l1Start - l2Start;
	//float t1;
	//t1 = (dot * Vec3Dot(l2, vec) - l2Len * Vec3Dot(l1, vec)) / 
	//	(l1Len * l2Len - )
	return 0.f;
}

bool Collision::HitSphere(const VECTOR3& l1Start, const VECTOR3& l1End,
	const VECTOR3& position, VECTOR3* outPosition)
{
	VECTOR3 v0 = position - l1Start;
	VECTOR3 d0 = CreateVec3(l1Start, l1End);
	Vec3Normalize(&d0, d0);
	float t0 = Vec3Dot(d0, v0);
	*outPosition = l1Start + t0 * d0;
	return true;
}

int Collision::MoveCheck(const VECTOR3& startPosition, const VECTOR3& endPosition, VECTOR3* outPosition)
{
	// レイピック
	VECTOR3 hitPosition, hitNormal;
	int materialIndex = RayPick(startPosition, endPosition, &hitPosition, &hitNormal);
	if (materialIndex == -1)
	{ // ヒットしなかったら移動後の位置は終点
		*outPosition = endPosition;
		return materialIndex;
	}
	// 壁をつきぬけたベクトル
	DirectX::XMVECTOR start = DirectX::XMLoadFloat3(&startPosition);
	DirectX::XMVECTOR end = DirectX::XMLoadFloat3(&endPosition);
	DirectX::XMVECTOR vec = DirectX::XMVectorSubtract(end, start);
	// 壁の法線ベクトルを単位化
	DirectX::XMVECTOR normal = DirectX::XMVector3Normalize(DirectX::XMLoadFloat3(&hitNormal));
	// 入射ベクトルを法線に射影
	DirectX::XMVECTOR dot = DirectX::XMVector3Dot(DirectX::XMVectorNegate(vec), normal);

	// 補正位置の計算
	DirectX::XMVECTOR position = DirectX::XMVectorMultiplyAdd(normal, dot, end);
	DirectX::XMStoreFloat3(outPosition, position);
	// 補正後の位置が壁にめり込んでいた場合は移動しないようにする
	if (-1 != RayPick(hitPosition, *outPosition, &hitPosition, &hitNormal))
	{
		*outPosition = startPosition;
	}
	return materialIndex;

}

