
#include "blend_state.h"
#include <assert.h>

HRESULT BlendState::CreateBlendState(ID3D11Device* device, ID3D11DeviceContext* context)
{
    HRESULT hr = S_OK;

    D3D11_BLEND_DESC bd;

    for (int state = 0; state < BLEND_TYPE; state++) {
        switch (state) {
        case BS_NONE:
            ZeroMemory(&bd, sizeof(bd));
            bd.IndependentBlendEnable = false;
            bd.AlphaToCoverageEnable = false;
            bd.RenderTarget[0].BlendEnable = false;
            bd.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
            bd.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
            bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

            bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
            bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
            bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
            bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

            break;

        case BS_ALPHA:
            ZeroMemory(&bd, sizeof(bd));
            bd.IndependentBlendEnable = false;
            bd.AlphaToCoverageEnable = false;
            bd.RenderTarget[0].BlendEnable = true;
            bd.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
            bd.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
            bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

            bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
            bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
            bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
            bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

            break;

        case BS_ADD:
            ZeroMemory(&bd, sizeof(bd));
            bd.IndependentBlendEnable = false;
            bd.AlphaToCoverageEnable = false;
            bd.RenderTarget[0].BlendEnable = true;
            bd.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
            bd.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
            bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

            bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
            bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
            bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
            bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

            break;

        case BS_SUBTRACT:
            ZeroMemory(&bd, sizeof(bd));
            bd.IndependentBlendEnable = false;
            bd.AlphaToCoverageEnable = false;
            bd.RenderTarget[0].BlendEnable = true;
            bd.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
            bd.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
            bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_REV_SUBTRACT;

            bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
            bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
            bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
            bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

            break;

        case BS_REPLACE:
            ZeroMemory(&bd, sizeof(bd));
            bd.IndependentBlendEnable = false;
            bd.AlphaToCoverageEnable = false;
            bd.RenderTarget[0].BlendEnable = true;
            bd.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
            bd.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
            bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

            bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_SRC_ALPHA;
            bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
            bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
            bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

            break;
        case BS_MULTIPLY:
            ZeroMemory(&bd, sizeof(bd));
            bd.IndependentBlendEnable = false;
            bd.AlphaToCoverageEnable = false;
            bd.RenderTarget[0].BlendEnable = true;
            bd.RenderTarget[0].SrcBlend = D3D11_BLEND_DEST_COLOR;
            bd.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
            bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

            bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_DEST_ALPHA;
            bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
            bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
            bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

            break;
        case BS_LIGHTEN:
            ZeroMemory(&bd, sizeof(bd));
            bd.IndependentBlendEnable = false;
            bd.AlphaToCoverageEnable = false;
            bd.RenderTarget[0].BlendEnable = true;
            bd.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
            bd.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
            bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_MAX;

            bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
            bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
            bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_MAX;
            bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

            break;

        case BS_DARKEN:
            ZeroMemory(&bd, sizeof(bd));
            bd.IndependentBlendEnable = false;
            bd.AlphaToCoverageEnable = false;
            bd.RenderTarget[0].BlendEnable = true;
            bd.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
            bd.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
            bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_MIN;

            bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
            bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
            bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_MIN;
            bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

            break;
        case BS_SCREEN:
            ZeroMemory(&bd, sizeof(bd));
            bd.IndependentBlendEnable = false;
            bd.AlphaToCoverageEnable = false;
            bd.RenderTarget[0].BlendEnable = true;
            bd.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
            bd.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_COLOR;
            bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

            bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
            bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
            bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
            bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

            break;

        }
            hr = device->CreateBlendState(
            &bd, blend_state[state].GetAddressOf());

        if (FAILED(hr))
        {
            assert(!"CreateBlendState");
        }
    }

    keep_context = context;

    return hr;
}

void BlendState::SetBlendState(const int blendMode, ID3D11DeviceContext* context)
{
    if (0 < blendMode || blendMode > BLEND_TYPE) return;
    if (now_blender == blendMode) return;
    if (context == nullptr) context = keep_context;

    context->OMSetBlendState(blend_state[blendMode].Get(), nullptr, 0xFFFFFFFF);
    now_blender = blendMode;
}