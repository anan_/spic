// UNIT.02
#pragma once

#include <fbxsdk.h> 
#include "model_data.h"

class FbxLoader
{
public:
	// FBXファイルからモデルデータ読み込み
	bool Load(const char* filename, ModelData& data);

private:
	// モデルデータを構築
	bool BuildModel(const char* dirName, FbxScene* fbxScene, ModelData& data);

	// ノードデータを構築
	void BuildNodes(FbxNode* fbxNode, ModelData& data, int parentNodeIndex);
	void BuildNode(FbxNode* fbxNode, ModelData& data, int parentNodeIndex);
	
	// メッシュデータを構築
	void BuildMeshes(FbxNode* fbxNode, ModelData& data);
	void BuildMesh(FbxNode* fbxNode, FbxMesh* fbxMesh, ModelData& data);

	// マテリアルデータを構築
	void BuildMaterials(const char* dirName, FbxScene* fbxScene, ModelData& data);
	void BuildMaterial(const char* dirName, FbxSurfaceMaterial* fbxSurfaceMaterial, ModelData& data);

	// アニメーションデータを構築
	void BuildAnimations(FbxScene* fbxScene, ModelData& data);

	// インデックスの検索
	int FindNodeIndex(ModelData& data, const char* name);
	int FindMaterialIndex(FbxScene* fbxScene, const FbxSurfaceMaterial* fbxSurfaceMaterial);
};
