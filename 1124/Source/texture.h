#pragma once

#include <d3d11.h>
#include <wrl.h>

using Microsoft::WRL::ComPtr;

class Texture
{
protected:
    ComPtr<ID3D11ShaderResourceView> shader_resouce_view;
    ComPtr<ID3D11SamplerState> sampler_state;
    ComPtr<ID3D11RenderTargetView> render_target_view;
    D3D11_TEXTURE2D_DESC texture2d_desc = {};

public:
    Texture() : shader_resouce_view(nullptr), sampler_state(nullptr)
    { }
    virtual ~Texture() { };
    bool Load(ID3D11Device* device, const wchar_t* filename = nullptr);
	bool Dummy(ID3D11Device* device);
    void Set(ID3D11DeviceContext* context, UINT Slot = 0, BOOL flg = true);
    UINT GetWidth() { return texture2d_desc.Width; }
    UINT GetHeight() { return texture2d_desc.Height; }
    bool Create(u_int width, u_int height, DXGI_FORMAT format);
    ID3D11RenderTargetView* GetRenderTarget() { return render_target_view.Get(); }
};
