
#pragma once

#include "scene.h"
#include <memory>
#include "font.h"

#include "vector.h"
#include "camera.h"
#include "constant_buffer.h"

#include "model.h"
#include "model_renderer.h"

#include "cube_mesh.h"

#include "obj3d.h"
#include "obj3d_manager.h"
#include "geometric_primitive.h"

#include "player.h"
#include "slime.h"
#include "stage.h"
#include "light.h"
#include "sprite.h"

class SceneGame : public Scene
{
private:
	std::shared_ptr<Camera> camera;

	std::unique_ptr<ModelRenderer> renderer;
	std::unique_ptr<Sprite> setumei_img;

	//std::unique_ptr<Obj3D> player;
	std::unique_ptr<Obj3D> field;
	std::unique_ptr<Obj3D> kitchen;
	std::unique_ptr<Obj3D> hand;
	std::shared_ptr<PlayerGun> playerGunMove;
	std::shared_ptr<PlayerBody> playerBodyMove;
	//std::shared_ptr<Slime> slimeMove01;
	static const int SlimeMax = 50;
	std::array<std::shared_ptr<Slime>, SlimeMax> slimeMove;
	std::shared_ptr<Stage> stageMove;
	std::shared_ptr<Stage> skyMove;
	std::unique_ptr<Obj3D> sky;

	std::unique_ptr<EraseSlime> eraseSlime;
	std::unique_ptr<ErasePlayerShot> erasePlayerShot;

	float time, currentSecond;
	int enemyNum, deadCount;
	bool clearFlg;

	struct CbLight
	{
		VECTOR4 lightColor;
		VECTOR4 lightDir;
		VECTOR4 ambientColor;
		VECTOR4 eyePos;

		POINTLIGHT PointLight[Light::POINTMAX];
		SPOTLIGHT SpotLight[Light::SPOTMAX];
	};
	std::unique_ptr<ConstantBuffer<CbLight>> lightBuffer;
	VECTOR3 light;
	// ���C�g
	VECTOR3 LightDir;

	void CreateLight(ID3D11Device* device);

public:
	void Init(ID3D11Device* device);
	void Release();
	void Update(ID3D11Device* device, float elapsedTime);
	void Render(ID3D11DeviceContext* context, float elapsedTime);

};