
#include "player_shot.h"
#include "enemy_manager.h"
#include "collision.h"

PlayerShot::PlayerShot()
{
    SetAcceleration(800.0f);
    SetDeceleration(0.0f);
    SetMaxSpeed(1000.0f);

    ezShot = std::make_unique<ErasePlayerShot>();
}

void PlayerShot::Move(Obj3D* obj, float elapsedTime)
{
    const VECTOR3& prev = obj->GetPosition();
    PlayerShotMove(obj, elapsedTime);

	//for (auto& enemy : *pEnemyManager.GetList())
	//{
	//	if (!enemy.GetExist()) continue;

	//	const VECTOR3& enePos = enemy.GetPosition();
	//	const VECTOR3& position = obj->GetPosition();

	//	//VECTOR3 start = prev, end = position;
	//	//if (-1 == Collision::RayPick(start, end, nullptr, nullptr))
	//	//{
	//	//	obj->SetEraseAlg(ezShot.get());
	//	//}
	//}

    VECTOR3 pos = obj->GetPosition();
    if ((pos.x >= 830.f || pos.z >= 680.f) ||
        (pos.x <= -750.f || pos.z <= -1440.f))
    {
        obj->SetEraseAlg(ezShot.get());
    }
}

void ErasePlayerShot::Erase(Obj3D* obj, float elapsedTime)
{
    obj->Release();
}
