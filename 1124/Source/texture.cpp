
#include "texture.h"
#include "create_DX11.h"
#include "resource_manager.h"
#include "WICTextureLoader.h"

using namespace GetCoreSystem;

bool Texture::Load(ID3D11Device* device, const wchar_t* filename)
{
    HRESULT hr = S_OK;

    //ComPtr<ID3D11Resource> resouce;
    //hr = DirectX::CreateWICTextureFromFile(GetDevice(),
    //    filename, resouce.GetAddressOf(), shader_resouce_view.GetAddressOf());
    //if (FAILED(hr))
    //{
    //    assert(!"CreateWICTextureFromFile");
    //    return false;
    //}

    //ComPtr<ID3D11Texture2D> texture2d;
    //resouce.Get()->QueryInterface(texture2d.GetAddressOf());
    //texture2d.Get()->GetDesc(&texture2d_desc);

    ResourceManager::LoadShaderResourceView(device, filename, shader_resouce_view.GetAddressOf(), &texture2d_desc);
   
    D3D11_SAMPLER_DESC sd;
    ZeroMemory(&sd, sizeof(sd));
    sd.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sd.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    sd.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    sd.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    sd.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sd.MinLOD = 0;
    sd.MaxLOD = D3D11_FLOAT32_MAX;

    hr = device->CreateSamplerState(&sd, sampler_state.GetAddressOf());
    if (FAILED(hr))
    {
        assert(!"CreateSamplerState");
    }

    return true;
}

bool Texture::Dummy(ID3D11Device* device)
{
	HRESULT hr = S_OK;

	texture2d_desc.Width = 1;
	texture2d_desc.Height = 1;
	texture2d_desc.MipLevels = 1;
	texture2d_desc.MiscFlags = 1;
	texture2d_desc.ArraySize = 1;
	texture2d_desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	texture2d_desc.SampleDesc.Count = 1;
	texture2d_desc.SampleDesc.Quality = 0;
	texture2d_desc.Usage = D3D11_USAGE_DYNAMIC;
	texture2d_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texture2d_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	texture2d_desc.MiscFlags = 0;

	ComPtr<ID3D11Texture2D> tex2d = nullptr;
	hr = device->CreateTexture2D(&texture2d_desc, nullptr, tex2d.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"CreateTexture2D");
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Format = texture2d_desc.Format;
	desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	desc.Texture2D.MostDetailedMip = 0;
	desc.Texture2D.MipLevels = texture2d_desc.MipLevels;
	hr = device->CreateShaderResourceView(tex2d.Get(), &desc, shader_resouce_view.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"shader_resource_view");
	}

	return true;
}

void Texture::Set(ID3D11DeviceContext* context, UINT Slot, BOOL flg)
{
    if (!flg)
    {
        ID3D11ShaderResourceView* srv[1] = { nullptr };
        ID3D11SamplerState* ss[1] = { nullptr };
        context->PSSetShaderResources(Slot, 1, srv);
        context->PSSetSamplers(Slot, 1, ss);
        context->DSSetShaderResources(Slot, 1, srv);
        context->DSSetSamplers(Slot, 1, ss);
    }
    if (shader_resouce_view)
    {
        context->PSSetShaderResources(Slot, 1, shader_resouce_view.GetAddressOf());
        context->PSSetSamplers(Slot, 1, sampler_state.GetAddressOf());
        context->DSSetShaderResources(Slot, 1, shader_resouce_view.GetAddressOf());
        context->DSSetSamplers(Slot, 1, sampler_state.GetAddressOf());
    }
}

bool Texture::Create(u_int width, u_int height, DXGI_FORMAT format)
{
    HRESULT hr = S_OK;

    ZeroMemory(&texture2d_desc, sizeof(texture2d_desc));
    texture2d_desc.Width = width;
    texture2d_desc.Height = height;
    texture2d_desc.MipLevels = 1;
    texture2d_desc.ArraySize = 1;
    texture2d_desc.Format = format;
    texture2d_desc.SampleDesc.Count = 1;
    texture2d_desc.Usage = D3D11_USAGE_DEFAULT;
    texture2d_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    texture2d_desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;

    ComPtr<ID3D11Texture2D> Texture2D;
    hr = GetDevice()->CreateTexture2D(&texture2d_desc, NULL, Texture2D.GetAddressOf());
    if (FAILED(hr))
    {
        assert(!"CreateTexture2D");
        return false;
    }

    D3D11_RENDER_TARGET_VIEW_DESC rtvd;
    ZeroMemory(&rtvd, sizeof(rtvd));
    rtvd.Format = format;
    rtvd.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
    rtvd.Texture2D.MipSlice = 0;

    hr = GetDevice()->CreateRenderTargetView(Texture2D.Get(), &rtvd, render_target_view.GetAddressOf());
    if (FAILED(hr))
    {
        assert(!"CreateRenderTargetView");
        return false;
    }

    D3D11_SHADER_RESOURCE_VIEW_DESC srvd;
    ZeroMemory(&srvd, sizeof(srvd));
    srvd.Format = format;
    srvd.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    srvd.Texture2D.MostDetailedMip = 0;
    srvd.Texture2D.MipLevels = 1;

    hr = GetDevice()->CreateShaderResourceView(Texture2D.Get(), &srvd, shader_resouce_view.GetAddressOf());
    if (FAILED(hr))
    {
        assert(!"CreateShaderResourceView");
        return false;
    }

    D3D11_SAMPLER_DESC sd;
    ZeroMemory(&sd, sizeof(sd));
    sd.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sd.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
    sd.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
    sd.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
    sd.ComparisonFunc = D3D11_COMPARISON_NEVER;
    //ボーダーカラー
    sd.BorderColor[0] = 1.0f;
    sd.BorderColor[1] = 1.0f;
    sd.BorderColor[2] = 1.0f;
    sd.BorderColor[3] = 1.0f;
    sd.MinLOD = 0;
    sd.MaxLOD = D3D11_FLOAT32_MAX;

    hr = GetDevice()->CreateSamplerState(&sd, sampler_state.GetAddressOf());
    if (FAILED(hr))
    {
        assert(!"CreateSamplerState");
        return false;
    }

    return true;
}
