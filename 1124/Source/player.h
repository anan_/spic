#pragma once

#include "actor.h"
#include <array>
#include "sound.h"
#include "player_shot.h"

class PlayerBody : public Actor
{
public:
    PlayerBody();
	~PlayerBody() {}
	void Move(Obj3D* obj, float elapsedTime);
};

class PlayerGun : public Actor
{
private:
    static const int shotMax = 50;
    std::array<std::unique_ptr<PlayerShot>, shotMax> shot;
	std::unique_ptr<Sound> shotSound;

public:
    PlayerGun();
    ~PlayerGun() {}
    void Move(Obj3D* obj, float elapsedTime);
};

class ErasePlayer : public EraseAlg
{
public:
	void Erase(Obj3D* obj, float elapsedTime)
	{
		obj->Release();
	}
};
