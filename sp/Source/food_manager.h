#pragma once

#include "obj3d_manager.h"

class FoodManager : public Obj3DManager
{
	static FoodManager* instance;

public:
	FoodManager(ID3D11Device* device) : Obj3DManager(device) {}
	~FoodManager() {}

    void Update(float elapsedTime);
    void Render(ID3D11DeviceContext* context, const FLOAT4X4& viewProjection) {}

public:
    static void Create(ID3D11Device* device)
    {
        if (instance != nullptr) return;
        instance = new FoodManager(device);
    }
    static FoodManager& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};

#define pFoodManager FoodManager::GetInstance()