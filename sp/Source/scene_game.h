
#pragma once

#include "scene.h"
#include <memory>
#include "font.h"

#include "vector.h"
#include "camera.h"
#include "constant_buffer.h"

#include "model.h"
#include "model_renderer.h"

#include "cube_mesh.h"

#include "obj3d.h"
#include "obj3d_manager.h"
#include "geometric_primitive.h"

#include "stage.h"
#include "light.h"
#include "sprite.h"

class SceneGame : public Scene
{
private:
	std::unique_ptr<ModelRenderer> renderer;
	std::unique_ptr<Sprite> setumei_img;

	std::unique_ptr<Obj3D> arrow;
	std::unique_ptr<Obj3D> field;
	std::unique_ptr<Obj3D> kitchen;

	std::shared_ptr<Stage> stageMove;
	std::shared_ptr<Stage> skyMove;
	std::unique_ptr<Obj3D> sky;

	float time, currentSecond;
	int enemyNum, deadCount;
	bool clearFlg;

	struct CbLight
	{
		VECTOR4 lightColor;
		VECTOR4 lightDir;
		VECTOR4 ambientColor;
		VECTOR4 eyePos;

		POINTLIGHT PointLight[Light::POINTMAX];
		SPOTLIGHT SpotLight[Light::SPOTMAX];
	};
	std::unique_ptr<ConstantBuffer<CbLight>> lightBuffer;
	VECTOR3 light;
	// ���C�g
	VECTOR3 LightDir;

	void LightInit(ID3D11Device* device);
	void LightUpdate(ID3D11DeviceContext* context);


public:
	void Init(ID3D11Device* device);
	void Release();
	void Update(ID3D11Device* device, float elapsedTime);
	void Render(ID3D11DeviceContext* context, float elapsedTime);

};