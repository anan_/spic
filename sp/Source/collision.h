#pragma once

#include "obj3d.h"

struct AABB
{
    VECTOR3 min;
    VECTOR3 max;
    VECTOR3 oldPos;

    void Set(const VECTOR3& pos, const VECTOR3& scale)
    {

    }
};

class Collision
{
private:
    static Obj3D* terrain;

public:
    static void RegisterTerrain(Obj3D* obj)
    {
        terrain = obj;
    }
    static void UnRegisterTerrain(Obj3D* obj)
    {

    }

	struct Sphere
	{
		VECTOR3 position;
		float scale;
		float radius;
	};
	static bool HitSphere(const Sphere& enemy, const Sphere& player);

	struct Capsule
	{
		VECTOR3 startPos;
		VECTOR3 endPos;
		float scale;
		float radius;
	};
	static bool HitCapsule(const Capsule& enemy, const Sphere& player);

    static int RayPick(
        const VECTOR3& start, const VECTOR3& end,
        VECTOR3* outPos, VECTOR3* outNormal)
    {
        return terrain->RayPick(start, end,
            outPos, outNormal);
    }

	// 点と直線
	static float CalculatePointLineDist(const VECTOR3& point,
		const VECTOR3& l1Start, const VECTOR3& l1End,
		float* length, VECTOR3* vec);
	// 点が線分に収まっているか
	static bool isSharpAngle(const VECTOR3& p1, const VECTOR3& p2, const VECTOR3& p3);
	// 点と線分
	static float CalculatePointSegmentDist(const VECTOR3& point,
		const VECTOR3& l1Start, const VECTOR3& l1End,
		float* length, VECTOR3* vec);

	// 円と縁
    static bool HitSphere(const VECTOR3& p1, float r1, const VECTOR3& p2, float r2);
	static bool HitSphere(const VECTOR3& l1Start, const VECTOR3& l1End, 
		const VECTOR3& position, VECTOR3* outPosition);

	static int MoveCheck(const VECTOR3& startPosition,
		const VECTOR3& endPosition,
		VECTOR3* outPosition);

	// ワールド座標(3D)からスクリーン座標(2D)へ変換
	static void WorldToScreen(VECTOR3* screenPosiotion, const VECTOR3& worldPosition);
	// スクリーン座標(2D)からワールド座標(3D)へ変換
	static void ScreenToWorld(VECTOR3* worldPosition, const VECTOR3& screenPosiotion);
};