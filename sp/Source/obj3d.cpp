
#include "obj3d.h"

int Obj3D::RayPick(const VECTOR3& startPosition, const VECTOR3& endPosition,
    VECTOR3* outPosition, VECTOR3* outNormal)
{
    DirectX::XMMATRIX worldTransform = DirectX::XMLoadFloat4x4(&world);
    DirectX::XMMATRIX inverseTransform = DirectX::XMMatrixInverse(nullptr, worldTransform);

    // オブジェクト空間でのレイに変換
    DirectX::XMVECTOR worldStart = DirectX::XMLoadFloat3(&startPosition);
    DirectX::XMVECTOR worldEnd = DirectX::XMLoadFloat3(&endPosition);
    DirectX::XMVECTOR localStart = DirectX::XMVector3TransformCoord(worldStart, inverseTransform);
    DirectX::XMVECTOR localEnd = DirectX::XMVector3TransformCoord(worldEnd, inverseTransform);

    // レイピック
    float outDistance;
    VECTOR3 start, end;
    DirectX::XMStoreFloat3(&start, localStart);
    DirectX::XMStoreFloat3(&end, localEnd);
    int ret = model->RayPick(start, end, outPosition, outNormal, &outDistance);
    if (ret != -1)
    {
        // オブジェクト空間からワールド空間へ変換
        DirectX::XMVECTOR localPosition = DirectX::XMLoadFloat3(outPosition);
        DirectX::XMVECTOR localNormal = DirectX::XMLoadFloat3(outNormal);
        DirectX::XMVECTOR worldPosition = DirectX::XMVector3TransformCoord(localPosition, worldTransform);
        DirectX::XMVECTOR worldNormal = DirectX::XMVector3TransformNormal(localNormal, worldTransform);

        DirectX::XMStoreFloat3(outPosition, worldPosition);
        DirectX::XMStoreFloat3(outNormal, worldNormal);
    }

    return ret;
}

void Obj3D::Update(float elapsedTime)
{
	prevPosition = position;
    CalculateWorld();

    model->UpdateAnimation(1.f / 60.f);
    model->CalculateLocalTransform();
    model->CalculateWorldTransform(DirectX::XMLoadFloat4x4(&world));

    if (mvAlg) mvAlg->Move(this, elapsedTime);
    if (ezAlg) ezAlg->Erase(this, elapsedTime);
}

void Obj3D::Release()
{
    model = nullptr;

    scale = {};
    rotation = {};
    position = {};
    world = {};

	prevPosition = {};

    exist = false;

    mvAlg = nullptr;
    ezAlg = nullptr;
}

void Obj3D::CalculateWorld()
{
    DirectX::XMMATRIX S, R, T, W;
    S = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
    R = DirectX::XMMatrixRotationRollPitchYaw(rotation.x, rotation.y, rotation.z);
    T = DirectX::XMMatrixTranslation(position.x, position.y, position.z);
    W = S * R * T;
    DirectX::XMStoreFloat4x4(&world, W);
}
