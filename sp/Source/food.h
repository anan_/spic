#pragma once

#include "actor.h"

enum class FoodNumber
{
	Chicken,

	MAX
};

class Food : Actor
{
public:
	Food();
	~Food() {}
	void Move(Obj3D* obj, float elapsedTime);

	int type;
	bool cook;
	void Cookink() {};
};

class EraseFood : public EraseAlg
{
public:
	void Erase(Obj3D* obj, float elapsedTime)
	{
		obj->Release();
	}
};
